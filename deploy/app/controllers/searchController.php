<?php
class Search extends Page
{
  public function index() {
    $this->setLayout(null);
    $this->setView(null);
    $this->setTitle(null);
    $this->setDescription(null);
    $this->setAnalytics(false);
  }

  private function data($query) {
    new CartolafcModel($this->_get('callback'), $query);
  }

  public function league() {
    $query = 'http://cartolafc.globo.com/ligas/buscar?nome=' . $this->_get('query');

    $this->data($query);
  }

  public function team() {
    $query = 'http://api.cartola.globo.com/time/busca.json?nome=' . $this->_get('query') . '&rows=20';

    $this->data($query);
  }
}
