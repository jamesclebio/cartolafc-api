<?php
class View extends Page
{
  public function index() {
    $this->setLayout(null);
    $this->setView(null);
    $this->setTitle(null);
    $this->setDescription(null);
    $this->setAnalytics(false);
  }

  private function data($query) {
    new CartolafcModel($this->_get('callback'), $query);
  }

  public function league() {
    $query = 'http://cartolafc.globo.com/liga/' . $this->_get('name') . '/times.json?page=' . $this->_get('page') . '&order_by=' . $this->_get('orderby');

    $this->data($query);
  }

  public function team() {
    $query = 'http://api.cartola.globo.com/time_adv/' . $this->_get('name') . '.json';

    $this->data($query);
  }

  public function selection() {
    $query = 'http://cartolafc.globo.com/selecao.json?esquema_id=4';

    $this->data($query);
  }
}
