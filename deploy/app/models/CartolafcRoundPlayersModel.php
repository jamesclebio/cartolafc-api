<?php
class CartolafcRoundPlayersModel
{
  private $parsePattern = '/.+"atletas": (\[.+?\]).+/';
  private $parseReplacement = '$1';

  public function __construct($callback, $query) {
    header('content-type: application/json; charset=utf-8');
    header("access-control-allow-origin: *");

    $this->fetch($query);
    $this->result($callback);
  }

  private function callback($name) {
    $identifier_syntax = '/^[$_\p{L}][$_\p{L}\p{Mn}\p{Mc}\p{Nd}\p{Pc}\x{200C}\x{200D}]*+$/u';
    $reserved_words = array('break', 'do', 'instanceof', 'typeof', 'case', 'else', 'new', 'var', 'catch', 'finally', 'return', 'void', 'continue', 'for', 'switch', 'while', 'debugger', 'function', 'this', 'with', 'default', 'if', 'throw', 'delete', 'in', 'try', 'class', 'enum', 'extends', 'super', 'const', 'export', 'import', 'implements', 'let', 'private', 'public', 'yield', 'interface', 'package', 'protected', 'static', 'null', 'true', 'false');

    return preg_match($identifier_syntax, $name) && !in_array(mb_strtolower($name, 'UTF-8'), $reserved_words);
  }

  private function result($callback) {
    if (!isset($callback)) {
      exit($this->result);
    }

    if ($this->callback($callback)) {
      exit("{$callback}($this->result)");
    }

    header('status: 400 Bad Request', true, 400);
  }

  private function fetch($query) {
    $this->data = curl_init();

    curl_setopt($this->data, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
    curl_setopt($this->data, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($this->data, CURLOPT_URL, $query);

    $this->result = curl_exec($this->data);

    curl_close($this->data);

    $this->parse();
  }

  private function parse() {
    if (preg_match($this->parsePattern, $this->result)) {
      $this->result = preg_replace($this->parsePattern . 's', $this->parseReplacement, $this->result);
    } else {
      $this->result = '{"errors": 1}';
    }
  }
}
