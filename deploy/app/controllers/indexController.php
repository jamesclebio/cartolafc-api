<?php
class Index extends Page
{
  public function index() {
    $this->setLayout(null);
    $this->setView(null);
    $this->setTitle(null);
    $this->setDescription(null);
    $this->setAnalytics(false);
  }
}
