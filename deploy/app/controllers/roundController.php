<?php
class Round extends Page
{
  public function index() {
    $this->setLayout(null);
    $this->setView(null);
    $this->setTitle(null);
    $this->setDescription(null);
    $this->setAnalytics(false);
  }

  private function data($query) {
    new CartolafcRoundPlayersModel($this->_get('callback'), $query);
  }

  public function players() {
    $query = 'http://globoesporte.globo.com/cartola-fc/';

    $this->data($query);
  }
}
